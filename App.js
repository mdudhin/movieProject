import React, {useState, useEffect} from 'react';
import {Provider} from 'react-redux';
import {store} from './redux/store';
import Rooter from './screens/Rooter';

export default function App() {
  return (
    <Provider store={store}>
      <Rooter />
    </Provider>
  );
}
