import React, {useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  FlatList,
  Animated,
} from 'react-native';
import Item from './ActorItem';

export default function Actor({actor}) {
  return (
    <View style={styles.container}>
      <FlatList
        data={actor}
        keyExtractor={(item, index) => `${index++}`}
        horizontal
        scrollEnabled
        showsHorizontalScrollIndicator={false}
        renderItem={({item}) => {
          return <Item item={item} />;
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
