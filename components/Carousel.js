import React, {useRef, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  FlatList,
  Animated,
} from 'react-native';
import Item from './CarouselItem';
import CarouselList from 'react-native-anchor-carousel';
import {useDispatch, useSelector} from 'react-redux';
import {carousel} from '../redux/Action/ShowMoviesAction';

const {height, width} = Dimensions.get('window');

export default function Carousel({navigation}) {
  const dispatch = useDispatch();
  const showCarousel = useSelector((state) => state.showMovies);
  const carouselRef = useRef(null);

  useEffect(() => {
    dispatch(carousel());
  }, [dispatch]);

  return (
    <View style={styles.container}>
      {/* <FlatList
        data={DATA}
        keyExtractor={(item) => item.id}
        horizontal
        pagingEnabled
        scrollEnabled
        showsHorizontalScrollIndicator={false}
        renderItem={({item}) => {
          return <Item item={item} />;
        }}
      /> */}
      <CarouselList
        style={styles.carousel}
        data={showCarousel.carousel}
        renderItem={({item}) => {
          return <Item item={item} navigation={navigation} />;
        }}
        itemWidth={200}
        inActiveOpacity={0.2}
        containerWidth={width - 25}
        ref={carouselRef}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: height / 2.7,
  },
  carousel: {
    flex: 1,
  },
});
