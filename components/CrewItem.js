import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

export default function CrewItem({item}) {
  return (
    <View style={styles.cardView}>
      <View>
        <Image style={styles.image} source={{uri: item.Image.url}} />
        <Text style={styles.job}>{item.job}</Text>
        <Text style={styles.name}>{item.name}</Text>
      </View>
    </View>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  cardView: {
    flex: 1,
    marginHorizontal: 15,
  },
  image: {
    width: 120,
    height: 120,
    borderRadius: 120 / 2,
    alignSelf: 'center',
  },
  job: {
    color: '#fff',
    fontSize: 13,
    letterSpacing: 1,
    fontFamily: 'Montserrat-Medium',
    alignSelf: 'center',
    marginTop: 7,
  },
  name: {
    color: '#fff',
    fontSize: 11,
    fontFamily: 'Montserrat-Light',
    alignSelf: 'center',
    marginTop: 5,
  },
});
