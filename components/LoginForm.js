import React from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Icon} from 'react-native-elements';

const LoginForm = ({
  email,
  wrongEmail,
  trueEmail,
  wrongEmailStatement,
  password,
  wrongPassword,
  wrongPasswordStatement,
  secureTextEntry,
  handleEmail,
  handlePassword,
  updateSecureTextEntry,
  login,
  isLoading,
}) => {
  const IconCheck = () => {
    if (trueEmail) {
      return (
        <Icon name="check-circle" type="feather" size={20} color="green" />
      );
    } else {
      return <View />;
    }
  };

  return (
    <View>
      <View style={styles.action}>
        <Icon name="user-o" type="font-awesome" size={20} />
        <TextInput
          placeholder="Enter Email"
          autoCapitalize="none"
          style={styles.textInput}
          keyboardType="email-address"
          onChangeText={(val) => handleEmail(val)}
        />
        {wrongEmail
          ? (console.log(wrongEmail),
            (
              <Icon
                name="alert-circle"
                type="feather"
                size={20}
                color="#d63031"
              />
            ))
          : (console.log(wrongEmail), (<IconCheck />))}
      </View>
      {wrongEmail ? (
        <Text style={styles.wrongStatement}>{wrongEmailStatement}</Text>
      ) : (
        <Text />
      )}
      <View style={styles.action}>
        <Icon name="lock" type="feather" size={20} />

        <TextInput
          placeholder="Enter Password"
          autoCapitalize="none"
          secureTextEntry={secureTextEntry ? true : false}
          onChangeText={(val) => handlePassword(val)}
          style={styles.textInput}
        />

        <TouchableOpacity onPress={updateSecureTextEntry}>
          {secureTextEntry ? (
            <Icon name="eye-off" type="feather" size={20} color="grey" />
          ) : (
            <Icon name="eye" type="feather" size={20} color="grey" />
          )}
        </TouchableOpacity>
      </View>
      {wrongPassword ? (
        <Text style={styles.wrongStatement}>{wrongPasswordStatement}</Text>
      ) : (
        <Text />
      )}
      <View style={styles.actionButton}>
        <TouchableOpacity>
          <Text style={styles.forgotPassword}>Forgot Password ?</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={login} style={styles.btn}>
          {isLoading ? (
            <ActivityIndicator size="small" color="#fff" />
          ) : (
            <Text style={styles.btnText}>Login</Text>
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LoginForm;

const styles = StyleSheet.create({
  action: {
    flexDirection: 'row',
    marginTop: 20,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: 'grey',
    paddingTop: 15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  textInput: {
    flex: 1,
    marginTop: -12,
    paddingLeft: 12,
    color: '#05375a',
  },
  wrongStatement: {
    paddingLeft: 20,
    color: '#d63031',
  },
  actionButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  forgotPassword: {
    paddingLeft: 20,
  },
  btn: {
    backgroundColor: 'rgba(219, 0, 0, 0.9)',
    paddingVertical: 10,
    paddingHorizontal: 40,
    borderRadius: 30,
    marginTop: 20,
  },
  btnText: {
    color: '#fff',
  },
  signUp: {
    flexDirection: 'row',
    alignSelf: 'center',
    paddingTop: 15,
  },
});
