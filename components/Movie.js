import React, {useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  FlatList,
  Animated,
} from 'react-native';
import Item from './MovieItem';
import {useDispatch, useSelector} from 'react-redux';
import {getMovies} from '../redux/Action/ShowMoviesAction';

export default function Movie({navigation}) {
  const dispatch = useDispatch();
  const showMoviesState = useSelector((state) => state.showMovies);

  useEffect(() => {
    dispatch(getMovies());
  }, [dispatch]);

  return (
    <View style={styles.container}>
      <FlatList
        data={showMoviesState.allMovies}
        keyExtractor={(item) => `${item.id}`}
        horizontal
        scrollEnabled
        showsHorizontalScrollIndicator={false}
        renderItem={({item}) => {
          return <Item item={item} navigation={navigation} />;
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  carousel: {
    flex: 1,
  },
});
