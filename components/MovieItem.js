import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

export default function MovieItem({item, navigation}) {
  return (
    <View style={styles.cardView}>
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('DetailMovie', {
            id: item.id,
          })
        }>
        <Image style={styles.image} source={{uri: item.Images[1].url}} />
      </TouchableOpacity>
    </View>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  cardView: {
    flex: 1,
    marginHorizontal: 10,
  },
  image: {
    width: 150,
    height: height / 4,
    borderRadius: 20,
    alignSelf: 'center',
  },
});
