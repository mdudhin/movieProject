import React, {useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  FlatList,
  Animated,
  ScrollView,
} from 'react-native';
import Item from './MyReviewItem';
import {useDispatch} from 'react-redux';
import {getMyReview} from '../redux/Action/ReviewMovieAction';

export default function MyReview({review}) {
  const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      <FlatList
        data={review}
        keyExtractor={(item) => `${item.id}`}
        refreshing={false}
        onRefresh={() => dispatch(getMyReview())}
        horizontal={false}
        onEndReachedThreshold={0.5}
        renderItem={({item}) => {
          return <Item item={item} />;
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 25,
  },
});
