import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';

const PhotoProfile = ({photoProfile, handleChoosePhoto}) => {
  const showUserState = useSelector((state) => state.showUser);

  return (
    <View>
      <Image
        source={
          photoProfile == null
            ? {
                uri: showUserState.user.image,
              }
            : {
                uri: photoProfile,
              }
        }
        style={styles.image}
      />
      <TouchableOpacity style={styles.btn} onPress={handleChoosePhoto}>
        <Text style={styles.imageText}>Pick Image</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PhotoProfile;

const styles = StyleSheet.create({
  btn: {
    paddingTop: 15,
  },
  imageText: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#2F80ED',
    textAlign: 'center',
  },
  image: {
    height: 130,
    width: 130,
    position: 'relative',
    borderRadius: 100,
    resizeMode: 'cover',
  },
});
