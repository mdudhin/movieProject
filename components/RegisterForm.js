import React, {useState} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Icon} from 'react-native-elements';

const RegisterForm = ({
  name,
  wrongName,
  trueName,
  email,
  wrongEmail,
  trueEmail,
  wrongEmailStatement,
  password,
  wrongPassword,
  wrongPasswordStatement,
  secureTextEntry,
  handleName,
  handleEmail,
  handlePassword,
  updateSecureTextEntry,
  register,
  isLoading,
}) => {
  const IconCheckName = () => {
    if (trueName) {
      return (
        <Icon name="check-circle" type="feather" size={20} color="green" />
      );
    } else {
      return <View />;
    }
  };

  const IconCheck = () => {
    if (trueEmail) {
      return (
        <Icon name="check-circle" type="feather" size={20} color="green" />
      );
    } else {
      return <View />;
    }
  };

  return (
    <View>
      <View style={styles.action}>
        <TextInput
          placeholder="Enter Name"
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={(val) => handleName(val)}
        />
        {wrongName ? (
          <Icon name="alert-circle" type="feather" size={20} color="#d63031" />
        ) : (
          <IconCheckName />
        )}
      </View>
      {wrongName ? (
        <Text style={styles.wrongStatement}>The form must be filled</Text>
      ) : (
        <Text />
      )}
      <View style={styles.action}>
        <Icon name="mail" type="feather" size={20} />
        <TextInput
          placeholder="Enter Email"
          autoCapitalize="none"
          style={styles.textInput}
          keyboardType="email-address"
          onChangeText={(val) => handleEmail(val)}
        />
        {wrongEmail ? (
          <Icon name="alert-circle" type="feather" size={20} color="#d63031" />
        ) : (
          <IconCheck />
        )}
      </View>
      {wrongEmail ? (
        <Text style={styles.wrongStatement}>{wrongEmailStatement}</Text>
      ) : (
        <Text />
      )}
      <View style={styles.action}>
        <Icon name="lock" type="feather" />
        <TextInput
          placeholder="Enter Password"
          autoCapitalize="none"
          secureTextEntry={secureTextEntry ? true : false}
          style={styles.textInput}
          onChangeText={(val) => handlePassword(val)}
        />
        <TouchableOpacity onPress={updateSecureTextEntry}>
          {secureTextEntry ? (
            <Icon name="eye-off" type="feather" size={20} color="grey" />
          ) : (
            <Icon name="eye" type="feather" size={20} color="grey" />
          )}
        </TouchableOpacity>
      </View>
      {wrongPassword ? (
        <Text style={styles.wrongStatement}>{wrongPasswordStatement}</Text>
      ) : (
        <Text />
      )}
      <View style={styles.actionButton}>
        <TouchableOpacity onPress={register} style={styles.btn}>
          {isLoading ? (
            <ActivityIndicator size="small" color="#fff" />
          ) : (
            <Text style={styles.btnText}>Register</Text>
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default RegisterForm;

const styles = StyleSheet.create({
  action: {
    flexDirection: 'row',
    marginTop: 15,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: 'grey',
    paddingTop: 15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  textInput: {
    flex: 1,
    marginTop: -12,
    paddingLeft: 12,
    color: '#05375a',
  },
  wrongStatement: {
    paddingLeft: 20,
    color: '#d63031',
  },
  actionButton: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingVertical: 15,
  },
  btn: {
    backgroundColor: 'rgba(219, 0, 0, 0.9)',
    paddingVertical: 10,
    paddingHorizontal: 40,
    borderRadius: 30,
  },
  btnText: {
    color: '#fff',
  },
  signIn: {
    flexDirection: 'row',
    alignSelf: 'center',
    paddingTop: 15,
  },
});
