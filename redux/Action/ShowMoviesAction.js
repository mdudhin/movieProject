import axios from 'axios';
import {
  GET_MOVIES,
  FAILED_GET_MOVIES,
  GET_UP_COMING_MOVIES,
  FAILED_GET_UP_COMING_MOVIES,
  GET_CAROUSEL,
  FAILED_CAROUSEL,
  MOVIES_PAGINATION,
  GET_MOVIE_BY_GENRE,
  FAILED_GET_MOVIE_BY_GENRE,
  CLEAR_MOVIE_BY_GENRE,
  GET_WATCH_LIST,
  FAILED_WATCH_LIST,
} from '../Type/showMovie';
import AsyncStorage from '@react-native-community/async-storage';

export const carousel = () => {
  return async (dispatch) => {
    try {
      const res = await axios.get(
        'https://ga-moviereview.herokuapp.com/api/v1/movie/upcoming/?limit=5&page=1',
      );
      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_CAROUSEL,
          data: data,
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({
        type: FAILED_CAROUSEL,
        message: e.response.data.message,
      });
    }
  };
};

export const getMovies = (page, movie) => {
  return async (dispatch) => {
    try {
      const res = await axios.get(
        `https://ga-moviereview.herokuapp.com/api/v1/movie/?limit=10&page=${page}`,
      );
      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_MOVIES,
          data: page === undefined ? data : [...movie, ...data],
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({type: FAILED_GET_MOVIES, message: e.response.data.message});
    }
  };
};

export const getUpComingMovies = (page, movie) => {
  return async (dispatch) => {
    try {
      const res = await axios.get(
        `https://ga-moviereview.herokuapp.com/api/v1/movie/upcoming/?limit=10&page=${page}`,
      );
      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_UP_COMING_MOVIES,
          data: page === undefined ? data : [...movie, ...data],
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({
        type: FAILED_GET_UP_COMING_MOVIES,
        message: e.response.data.message,
      });
    }
  };
};

export const getMovieByGenre = (genre, movie) => {
  return async (dispatch) => {
    try {
      const res = await axios.get(
        `https://ga-moviereview.herokuapp.com/api/v1/movie/genre/${genre}`,
      );
      if (res !== null) {
        const data = res.data.data.Movies;
        dispatch({
          type: GET_MOVIE_BY_GENRE,
          data: data,
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({
        type: FAILED_GET_MOVIE_BY_GENRE,
        message: e.response.data.message,
      });
    }
  };
};

export const getWatchList = () => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = await axios.get(
        'https://ga-moviereview.herokuapp.com/api/v1/movie/watchlist',
        {
          headers: {
            auth: token,
          },
        },
      );
      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_WATCH_LIST,
          data: data,
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({
        type: FAILED_WATCH_LIST,
        message: e.response.data.message,
      });
    }
  };
};
