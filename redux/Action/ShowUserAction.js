import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {
  GET_USER,
  FAILED_GET_USER,
  EDIT_USER,
  FAILED_EDIT_USER,
  SHOW_LOADING,
  CLEAR_RESPONSE,
} from '../Type/showUser';

export const getUser = () => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = await axios.get(
        'https://ga-moviereview.herokuapp.com/api/v1/user',
        {
          headers: {
            auth: token,
          },
        },
      );
      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_USER,
          user: data,
        });
      }
    } catch (e) {
      dispatch({type: FAILED_GET_USER, message: e.response.data.message});
    }
  };
};

export const editUser = (nameEdit, photoName, photoProfileEdit) => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const data = new FormData();
      data.append('name', nameEdit);
      data.append('image', {
        uri: photoProfileEdit,
        name: photoName,
        type: 'image/jpg',
      });
      axios
        .put('https://ga-moviereview.herokuapp.com/api/v1/user', data, {
          headers: {
            auth: token,
            'Content-Type': 'application/json',
          },
        })
        .then((res) => {
          if (res.data.status == 'success') {
            const message = 'Success to edit user !';
            dispatch({
              type: EDIT_USER,
              message: message,
            });
            dispatch(getUser());
          } else {
            dispatch({
              type: FAILED_EDIT_USER,
              message: res.data.message,
            });
          }
        })
        .catch((error) => {
          console.log(error);
          dispatch({
            type: FAILED_EDIT_USER,
            message: 'Failed to edit !',
          });
        });
    } catch (e) {
      console.log(e);
    }
  };
};

export const clearResponse = () => {
  return (dispatch) => {
    dispatch({type: CLEAR_RESPONSE});
  };
};
