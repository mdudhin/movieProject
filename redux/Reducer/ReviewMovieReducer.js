import {
  ADD_REVIEW,
  FAILED_ADD_REVIEW,
  HIDE_RESPONSE_REVIEW,
  GET_MY_REVIEW,
  FAILED_GET_MY_REVIEW,
  EDIT_REVIEW,
  FAILED_EDIT_REVIEW,
  DELETE_REVIEW,
  FAILED_DELETE_REVIEW,
} from '../Type/reviewMovie';

const initialState = {
  myReview: [],
  message: null,
  isResponse: false,
};

export const ReviewMovieReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_REVIEW:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    case FAILED_ADD_REVIEW:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    case EDIT_REVIEW:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    case FAILED_EDIT_REVIEW:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    case DELETE_REVIEW:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    case FAILED_DELETE_REVIEW:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    case HIDE_RESPONSE_REVIEW:
      return {
        ...state,
        isResponse: false,
      };
    case GET_MY_REVIEW:
      return {
        ...state,
        myReview: action.myReview,
      };
    case FAILED_GET_MY_REVIEW:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    default:
      return state;
  }
};
