import {
  GET_MOVIES,
  FAILED_GET_MOVIES,
  GET_UP_COMING_MOVIES,
  FAILED_GET_UP_COMING_MOVIES,
  GET_CAROUSEL,
  FAILED_CAROUSEL,
  GET_MOVIE_BY_GENRE,
  FAILED_GET_MOVIE_BY_GENRE,
  CLEAR_MOVIE_BY_GENRE,
  GET_WATCH_LIST,
  FAILED_WATCH_LIST,
} from '../Type/showMovie';

const initialState = {
  carousel: [],
  allMovies: [],
  upComingMovies: [],
  movieByGenre: [],
  watchList: [],
  message: null,
};

export const ShowMoviesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CAROUSEL:
      return {
        ...state,
        carousel: action.data,
      };
    case FAILED_CAROUSEL:
      return {
        ...state,
        message: action.message,
      };
    case GET_MOVIES:
      return {
        ...state,
        allMovies: action.data,
      };
    case FAILED_GET_MOVIES:
      return {
        ...state,
        message: action.message,
      };
    case GET_UP_COMING_MOVIES:
      return {
        ...state,
        upComingMovies: action.data,
      };
    case FAILED_GET_UP_COMING_MOVIES:
      return {
        ...state,
        message: action.message,
      };
    case GET_MOVIE_BY_GENRE:
      return {
        ...state,
        movieByGenre: action.data,
      };
    case FAILED_GET_MOVIE_BY_GENRE:
      return {
        ...state,
        message: action.message,
      };
    case GET_WATCH_LIST:
      return {
        ...state,
        watchList: action.data,
      };
    case FAILED_WATCH_LIST:
      return {
        ...state,
        message: action.message,
      };
    default:
      return state;
  }
};
