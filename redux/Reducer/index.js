import {combineReducers} from 'redux';
import {AuthReducer} from './AuthReducer';
import {ShowMoviesReducer} from './ShowMoviesReducer';
import {DetailMovieReducer} from './DetailMovieReducer';
import {ReviewMovieReducer} from './ReviewMovieReducer';
import {ShowUserReducer} from './ShowUserReducer';

export default combineReducers({
  auth: AuthReducer,
  showMovies: ShowMoviesReducer,
  detailMovie: DetailMovieReducer,
  reviewMovie: ReviewMovieReducer,
  showUser: ShowUserReducer,
});
