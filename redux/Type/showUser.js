export const GET_USER = 'GET_USER';
export const FAILED_GET_USER = 'FAILED_GET_USER';
export const EDIT_USER = 'EDIT_USER';
export const FAILED_EDIT_USER = 'FAILED_EDIT_USER';
export const SHOW_LOADING = 'SHOW_LOADING';
export const SHOW_RESPONSE = 'SHOW_RESPONSE';
export const CLEAR_RESPONSE = 'CLEAR_RESPONSE';
