import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import AllGenre from '../../components/AllGenre';

export default function AllGenreScreen({navigation}) {
  return (
    <View style={styles.container}>
      <AllGenre navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(12, 15, 17)',
    paddingHorizontal: 7,
  },
});
