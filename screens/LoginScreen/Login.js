import React, {useState, useRef, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  ToastAndroid,
  TouchableWithoutFeedback,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {Icon} from 'react-native-elements';
import LoginForm from '../../components/LoginForm';
import {useDispatch, useSelector} from 'react-redux';
import {handleLogin, handleResponse} from '../../redux/Action/AuthAction';

export default function Login({AnimationRef, onStateChange}) {
  const dispatch = useDispatch();
  const AuthState = useSelector((state) => state.auth);

  const [email, setEmail] = useState('');
  const [wrongEmail, setWrongEmail] = useState(false);
  const [trueEmail, setTrueEmail] = useState(false);
  const [wrongEmailStatement, setWrongEmailStatement] = useState(false);

  const [password, setPassword] = useState('');
  const [wrongPassword, setWrongPassword] = useState(false);
  const [wrongPasswordStatement, setWrongPasswordStatement] = useState(false);
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const handleEmail = (val) => {
    setEmail(val);

    if (email != '') {
      setWrongEmail(false);
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (regex.test(email)) {
      console.log('regex', regex.test(email));
      setWrongEmail(false);
      setTrueEmail(true);
    } else {
      console.log('regex gagal', regex.test(email));
      setWrongEmail(true);
      setWrongEmailStatement('Wrong email format');
    }
  };

  const handlePassword = (val) => {
    setPassword(val);
    if (password != '') {
      setWrongPassword(false);
    }
  };

  const updateSecureTextEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const login = async () => {
    if (email == '') {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Email must be filled');
    } else {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    }

    if (password == '') {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be filled');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (email != '' && password != '' && regex.test(email)) {
      console.log('benar', email, password);
      dispatch(handleLogin(email, password));
    } else {
      console.log('error');
    }
  };

  if (AuthState.isResponse) {
    ToastAndroid.show(AuthState.message, ToastAndroid.SHORT);
    dispatch(handleResponse());
  }

  return (
    <Animatable.View
      style={styles.content}
      animation="fadeInUp"
      ref={AnimationRef}>
      <TouchableWithoutFeedback onPress={onStateChange}>
        <Animatable.View style={styles.closeBtn}>
          <Icon name="x" type="feather" size={30} />
        </Animatable.View>
      </TouchableWithoutFeedback>
      <LoginForm
        email={email}
        wrongEmail={wrongEmail}
        trueEmail={trueEmail}
        wrongEmailStatement={wrongEmailStatement}
        password={password}
        wrongPassword={wrongPassword}
        wrongPasswordStatement={wrongPasswordStatement}
        secureTextEntry={secureTextEntry}
        handleEmail={handleEmail}
        handlePassword={handlePassword}
        updateSecureTextEntry={updateSecureTextEntry}
        login={login}
        isLoading={AuthState.isLoading}
      />
    </Animatable.View>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  content: {
    height: height / 2.5,
    backgroundColor: 'white',
    marginLeft: 4,
    paddingHorizontal: 40,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  closeBtn: {
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    width: 50,
    height: 50,
    borderRadius: 30,
    marginTop: -25,
  },
});
