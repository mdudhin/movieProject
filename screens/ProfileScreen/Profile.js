import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  ToastAndroid,
  Alert,
  Text,
} from 'react-native';
import PhotoProfile from '../../components/PhotoProfile';
import ProfileForm from '../../components/ProfileForm';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import {getUser, editUser} from '../../redux/Action/ShowUserAction';

const Profile = ({navigation}) => {
  const dispatch = useDispatch();
  const showUserState = useSelector((state) => state.showUser);

  const [userId, setUserId] = useState('');
  const [photoProfile, setPhotoProfile] = useState(null);
  const [photoName, setPhotoName] = useState(null);
  const [photoPath, setPhotoPath] = useState(null);

  const [name, setName] = useState(null);
  const [wrongName, setWrongName] = useState(false);

  useEffect(() => {
    // getUser();
    dispatch(getUser());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChoosePhoto = () => {
    const options = {
      title: 'Select Photo',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('Image picker error = ', response.error);
      } else {
        setPhotoProfile(response.uri);
        setPhotoName(response.fileName);
        setPhotoPath(response);
        // uploadImage();
      }
    });
  };

  const handleName = (val) => {
    setName(val);
    if (name != '') {
      setWrongName(false);
    }
  };

  const updateProfile = () => {
    const nameEdit = name == null ? showUserState.user.name : name;
    const photoProfileEdit =
      photoProfile == null ? showUserState.user.image : photoProfile;
    dispatch(editUser(nameEdit, photoName, photoProfileEdit));
    ToastAndroid.show('Edit Profile', ToastAndroid.SHORT);
  };

  return (
    <KeyboardAvoidingView style={styles.container} behavior="height">
      <View style={styles.header}>
        <PhotoProfile
          photoProfile={photoProfile}
          handleChoosePhoto={handleChoosePhoto}
        />
      </View>
      <View style={styles.content}>
        <ProfileForm
          userId={userId}
          name={name}
          photoProfile={photoProfile}
          photoName={photoName}
          wrongName={wrongName}
          isLoading={showUserState.isLoading}
          handleName={handleName}
          updateProfile={updateProfile}
        />
      </View>
      <View style={styles.footer} />
    </KeyboardAvoidingView>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(12, 15, 17)',
    justifyContent: 'center',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 50,
    paddingHorizontal: 20,
    paddingBottom: 30,
  },
  content: {
    paddingHorizontal: 20,
    paddingVertical: 30,
    marginLeft: 20,
    marginRight: 20,
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
});
