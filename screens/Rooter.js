import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {Icon} from 'react-native-elements';
import SplashScreen from './SplashScreen/Splash';
import HomeScreen from './HomeScreen/Home';
import DetailMovieScreen from './DetailMovieScreen/DetailMovieScreen';
import VideoPlayerScreen from './VideoPlayerScreen/VideoPlayerScreen';
import DetailWatchListScreen from './DetailWatchListScreen/DetailWatchListScreen';
import AllGenreScreen from './AllGenreScreen/AllGenreScreen';
import ShowMovieByGenreScreen from './ShowMovieByGenreScreen/ShowMovieByGenreScreen';
import ShowMovieScreen from './ShowMovieScreen/ShowMovieScreen';
import ShowUpComingScreen from './ShowUpComingScreen/ShowUpComingScreen';
import WatchListScreen from './WatchListScreen/WatchListScreen';
import ProfileScreen from './ProfileScreen/Profile';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerContent from '../components/DrawerContent';
import {useSelector, useDispatch} from 'react-redux';
import {handleCheckToken} from '../redux/Action/AuthAction';
import MovieReviewScreen from './MovieReviewScreen/MovieReviewScreen';
import MyReviewScreen from './MyReviewScreen/MyReviewScreen';

export default function Rooter() {
  const AuthState = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      dispatch(handleCheckToken());
      setIsLoading(false);
    }, 1000);
  }, [dispatch]);

  const Loading = () => {
    return (
      <View style={styles.loadingScreen}>
        <ActivityIndicator size="large" />
      </View>
    );
  };

  if (isLoading) {
    return <Loading />;
  }

  const HomeStack = createStackNavigator();
  const HomeStackScreen = ({navigation}) => {
    return (
      <HomeStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: 'rgb(12, 15, 17)',
            elevation: 0,
            shadowOpacity: 0,
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <HomeStack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
        <HomeStack.Screen
          name="DetailMovie"
          component={DetailMovieScreen}
          options={{headerShown: false}}
        />
        <HomeStack.Screen
          name="VideoPlayer"
          component={VideoPlayerScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
          }}
        />
        <HomeStack.Screen
          name="MovieReview"
          component={MovieReviewScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
          }}
        />
        <HomeStack.Screen
          name="AllGenre"
          component={AllGenreScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
          }}
        />
        <HomeStack.Screen
          name="MovieByGenre"
          component={ShowMovieByGenreScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
          }}
        />
        <HomeStack.Screen
          name="AllMovies"
          component={ShowMovieScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
          }}
        />
        <HomeStack.Screen
          name="UpComingMovies"
          component={ShowUpComingScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
          }}
        />
      </HomeStack.Navigator>
    );
  };

  const WatchListStack = createStackNavigator();
  const WatchListStackScreen = ({navigation}) => {
    return (
      <WatchListStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: 'rgb(12, 15, 17)',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <WatchListStack.Screen
          name="WatchList"
          component={WatchListScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
        <WatchListStack.Screen
          name="DetailWatchList"
          component={DetailWatchListScreen}
          options={{headerShown: false}}
        />
        <WatchListStack.Screen
          name="MovieReview"
          component={MovieReviewScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
          }}
        />
        <WatchListStack.Screen
          name="VideoPlayer"
          component={VideoPlayerScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
          }}
        />
      </WatchListStack.Navigator>
    );
  };

  const MyReviewStack = createStackNavigator();
  const MyReviewStackScreen = ({navigation}) => {
    return (
      <MyReviewStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: 'rgb(12, 15, 17)',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <MyReviewStack.Screen
          name="MyReview"
          component={MyReviewScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
      </MyReviewStack.Navigator>
    );
  };

  const ProfileStack = createStackNavigator();
  const ProfileStackScreen = ({navigation}) => {
    return (
      <ProfileStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: 'rgb(12, 15, 17)',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <ProfileStack.Screen
          name="Completion"
          component={ProfileScreen}
          options={{
            title: 'HEFLIX',
            headerTitleStyle: {color: '#DB0000', fontSize: 25},
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
      </ProfileStack.Navigator>
    );
  };

  const Drawer = createDrawerNavigator();
  const DrawerScreen = () => {
    return (
      <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
        <Drawer.Screen
          name="Home"
          component={HomeStackScreen}
          options={{
            drawerLabel: () => <Text style={styles.text}>Home</Text>,
            drawerIcon: () => (
              <Icon name="home" type="feather" size={23} color="grey" />
            ),
          }}
        />
        <Drawer.Screen
          name="Watch List"
          component={WatchListStackScreen}
          options={{
            drawerLabel: () => <Text style={styles.text}>Watch List</Text>,
            drawerIcon: () => (
              <Icon
                name="watch-later"
                type="materialicons"
                size={23}
                color="grey"
              />
            ),
          }}
        />
        <Drawer.Screen
          name="MyReview"
          component={MyReviewStackScreen}
          options={{
            drawerLabel: () => <Text style={styles.text}>My Review</Text>,
            drawerIcon: () => (
              <Icon
                name="rate-review"
                type="materialicons"
                size={23}
                color="grey"
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Profile"
          component={ProfileStackScreen}
          options={{
            drawerLabel: () => <Text style={styles.text}>Profile</Text>,
            drawerIcon: () => (
              <Icon name="user" type="feather" size={23} color="grey" />
            ),
          }}
        />
      </Drawer.Navigator>
    );
  };

  const RootStack = createStackNavigator();
  return (
    <NavigationContainer>
      <RootStack.Navigator>
        {!AuthState.token ? (
          <RootStack.Screen
            name="Auth"
            component={SplashScreen}
            options={{headerShown: false}}
          />
        ) : (
          <RootStack.Screen
            name="App"
            component={DrawerScreen}
            options={{headerShown: false}}
          />
        )}
      </RootStack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  loadingScreen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'grey',
  },
});
