import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import AllMovies from '../../components/AllMovies';
import {useDispatch, useSelector} from 'react-redux';
import {getMovies} from '../../redux/Action/ShowMoviesAction';

export default function ShowMovieScreen({navigation}) {
  const dispatch = useDispatch();
  const showMoviesState = useSelector((state) => state.showMovies);
  const [page, setPage] = useState(2);

  useEffect(() => {
    dispatch(getMovies());
  }, [dispatch]);

  const movie = showMoviesState.allMovies;

  const handleLoadMore = () => {
    const pagePlus = page + 1;
    setPage(pagePlus);
    dispatch(getMovies(page, movie));
  };

  return (
    <View style={styles.container}>
      <AllMovies
        showMoviesState={showMoviesState.allMovies}
        handleLoadMore={handleLoadMore}
        navigation={navigation}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(12, 15, 17)',
    paddingHorizontal: 7,
  },
});
