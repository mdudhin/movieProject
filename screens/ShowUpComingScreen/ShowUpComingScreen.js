import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import AllMovies from '../../components/AllMovies';
import {useDispatch, useSelector} from 'react-redux';
import {getUpComingMovies} from '../../redux/Action/ShowMoviesAction';

export default function ShowUpComingScreen({navigation}) {
  const dispatch = useDispatch();
  const showMoviesState = useSelector((state) => state.showMovies);
  const [page, setPage] = useState(2);

  useEffect(() => {
    dispatch(getUpComingMovies());
  }, [dispatch]);

  const movie = showMoviesState.upComingMovies;

  const handleLoadMore = () => {
    const pagePlus = page + 1;
    setPage(pagePlus);
    dispatch(getUpComingMovies(page, movie));
  };

  return (
    <View style={styles.container}>
      <AllMovies
        showMoviesState={showMoviesState.upComingMovies}
        handleLoadMore={handleLoadMore}
        navigation={navigation}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(12, 15, 17)',
    paddingHorizontal: 7,
  },
});
