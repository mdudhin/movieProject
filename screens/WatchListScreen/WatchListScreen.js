import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import WatchList from '../../components/WatchList';
import {useDispatch, useSelector} from 'react-redux';
import {getMovies, getWatchList} from '../../redux/Action/ShowMoviesAction';

export default function WatchListScreen({navigation}) {
  const dispatch = useDispatch();
  const showMoviesState = useSelector((state) => state.showMovies);
  const [page, setPage] = useState(2);

  useEffect(() => {
    dispatch(getWatchList());
  }, [dispatch]);

  const movie = showMoviesState.watchList;

  return (
    <View style={styles.container}>
      <WatchList showMoviesState={movie} navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(12, 15, 17)',
    paddingHorizontal: 7,
  },
});
